# -*- coding: utf-8 -*-
"""
Edit the sample rate in wav format header

The new sample rate will be: 7,817
The old one: 8000

Created on 2019-10-04
@author: atoultaro
"""
import os
import glob


def change_samplerate(target_file, output_folder):
    # find file basename
    target_file_base = os.path.basename(target_file)
    print(target_file_base)

    # build output files' full path
    sound_new_wav = os.path.join(output_folder, target_file_base)

    # output new sound file with new sample rate
    new_file = bytearray()
    with open(target_file, 'rb') as f:
        header_front = f.read(24)  # header before samplerate
        header_sample_rate = f.read(4)  # samplerate
        header_back = f.read(16)  # header after samplerate

        binarySound = f.read()
        # old_samplerate = int.from_bytes(header_sample_rate, byteorder='little')
        header_sample_rate_new = new_samplerate.to_bytes(4, byteorder="little")

        new_file = header_front + header_sample_rate_new + header_back + \
                   binarySound

        with open(sound_new_wav, "wb") as n:
            print("Writing to "+sound_new_wav)
            n.write(new_file)
            return True


new_samplerate = 7817
# input_deploy = ['Congo_2017/biss_jan2017', 'Congo_2017/biss_mar2017', 'Congo_2016/biss_nov2016', 'Gabon_2016/jobo_nov2016', 'Congo_2016/madj_nov2016', 'Congo_2017/mool_mar2017']
input_deploy = ['Congo_2017/biss_mar2017', 'Congo_2016/biss_nov2016', 'Gabon_2016/jobo_nov2016', 'Congo_2016/madj_nov2016', 'Congo_2017/mool_mar2017']

input_path = '/mnt/drive_L/ELP/SOUND_FILES'
for dd in input_deploy:
    print('Deployment: '+dd)
    input_folder = os.path.join(input_path, dd)
    output_path, input_folder_base = os.path.split(input_folder)
    output_folder = os.path.join(output_path, input_folder_base+'_new_samplerate')

    # create output_folder if not exists
    if not os.path.exists(output_folder):
        os.mkdir(output_folder)

    # find all wav filenames in input_folder
    target_file_list = sorted(glob.glob(os.path.join(input_folder, '*.wav')))

    for target_file in target_file_list:
        if_work = change_samplerate(target_file, output_folder)
        if if_work is True:
            print("Finish writing to the file: "+os.path.basename(target_file))

        # # find file basename
        # target_file_base = os.path.basename(target_file)
        # print(target_file_base)
        #
        # # build output files' full path
        # sound_new_wav = os.path.join(output_folder, target_file_base)
        #
        # # output new sound file with new sample rate
        # new_file = bytearray()
        # with open(target_file, 'rb') as f:
        #     header_front = f.read(24)  # header before samplerate
        #     header_sample_rate = f.read(4)  # samplerate
        #     header_back = f.read(16)  # header after samplerate
        #
        #     binarySound = f.read()
        #     # old_samplerate = int.from_bytes(header_sample_rate, byteorder='little')
        #     header_sample_rate_new = new_samplerate.to_bytes(4, byteorder="little")
        #
        #     new_file = header_front + header_sample_rate_new + header_back + \
        #                binarySound
        #
        #     with open(sound_new_wav, "wb") as n:
        #         print("Writing to "+sound_new_wav)
        #         n.write(new_file)
