# __elp_sound_wrong_header

The script here aims to fix the wrong sampling rate in the wav file header.
It reads binary data from a wav file, replace the old sampling rate by a new one,
write everything back to create a new wav file.

When writing to files needs root permission, use the following command in the commmand line, instead of running
edit_header.py in IDE or command line:
sudo venv/bin/python edit_header.py